﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            Console.WriteLine("button pressed");

            string x = textBox1.Text;
            double quantity = Double.Parse(x);
            double price = 10.00;
            double totalCost = 0;

            
           if(checkBox1.Checked == true)
            {
                totalCost = (price+2) * quantity;
                Console.WriteLine("total cost" + totalCost);
            }
            else
            {
                totalCost = price * quantity;
                Console.WriteLine("total cost" + totalCost);
            }
           
            Form2 screen2 = new Form2(label1.Text,label2.Text,x);
            screen2.ShowDialog();
        }
    }
}
